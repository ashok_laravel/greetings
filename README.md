

## Structure

If any of the following are applicable to your project, then the directory structure should follow industry best practices by being named the following.

```
your_Laravel_project/
vendor/
```


## Install

Via Composer

``` bash
$ composer require ashok/unique-code
```

## Usage

``` php laravel 

use Ashok\Unique\Unique;



1.  
return  Unique::generate(10);

output : aZKEqEYGJb

2. 

return  Unique::generate(10,'MyUniqueCode');

output : aZKEqEYGJbMyUniqueCode

3. 
  return  Unique::getRandomInteger(1000,5000);

Output : 3709

```

## Change log

Please see logs https://gitlab.com/ashok_laravel/greetings.git for more information on what has changed recently.




## Security

If you discover any security related issues, please email kumarashok30592@gmail.com instead of using the issue tracker.

## Credits

- [Yogesh]
- [Tarun]

