<?php

namespace Ashok\Unique;

class Unique
{
    /** @var string */
    static $alphabet;

    /** @var int */
    static $alphabetLength;


    /**
     * @param string $alphabet
     */
    public static function setAlphabet($alphabet)
    {
        self::$alphabet = $alphabet;
        self::$alphabetLength = strlen($alphabet);
    }

    /**
     * @param int $length
     * @return string
     */
    public static function generate($length,$string=null)
    {
        
        $token = '';
        self::setAlphabet(
            implode(range('a', 'z'))
                . implode(range('A', 'Z'))
                . implode(range(0, 9))
        );
        for ($i = 0; $i < $length; $i++) {
             $randomKey = self::getRandomInteger(0, self::$alphabetLength);
            $token .= self::$alphabet[$randomKey];
        }

        return $token.$string;
    }

    /**
     * @param int $min
     * @param int $max
     * @return int
     */
    public static function getRandomInteger($min, $max)
    {
        $range = ($max - $min);

        if ($range < 0) {
            // Not so random...
            return $min;
        }

        $log = log($range, 2);

        // Length in bytes.
        $bytes = (int) ($log / 8) + 1;

        // Length in bits.
        $bits = (int) $log + 1;

        // Set all lower bits to 1.
        $filter = (int) (1 << $bits) - 1;

        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));

            // Discard irrelevant bits.
            $rnd = $rnd & $filter;
        } while ($rnd >= $range);

        return ($min + $rnd);
    }
}
